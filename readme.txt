INTRODUCTION
6.x-1.0-beta
===============================================================================

Domain background enables site editors and content builders to change the
background image or color of a domain on Domain Access-based websites.
Requires use of Domain Configuration and Domain Settings sub-modules.
This module extends the large number of contributed modules that work with

A list of other compatible Domain Access modules can be found here:
http://drupal.org/node/1068570

Potential future upgrades might include Context integration, dynamic
backgrounds, and support for inline CSS.

Sponsored and maintained by the California Institute of the Arts
(http://calarts.edu/)



INSTALLATION & CONFIGURATION
===============================================================================

Domain Background requires that the following modules be enabled as part of the
Domain Access module.

Domain Access: http://drupal.org/project/domain
    + Domain Settings
    + Domain Configuration

Once installed, the administration page can be found here:
admin/build/themes/domain_background

